<?php

declare(strict_types=1);
error_reporting(E_ALL);
ini_set('display_errors', '1');

use Training\Command\DummyCommand;
use Training\Command\GreetingCommand;
use Training\Manager\GreetingManager;
use Training\Output\Output;

require_once 'vendor/autoload.php';

$output = new Output();

switch ($argv[1] ?? '') {
    case 'dummy':
        $command = new DummyCommand($output);
        $result = $command->execute();
        break;
    case 'greet':
        $command = new GreetingCommand($output, new GreetingManager());
        $name = $argv[2] ?? '';
        $yell = ('yell' === strtolower($argv[3] ?? ''));
        $result = $command->execute($name, $yell);
        break;
    default:
        $output->writeln('Unknown command.');
        $result = 2;
}

exit($result ?? 3);
