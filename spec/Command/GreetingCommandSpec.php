<?php

declare(strict_types=1);

namespace spec\Training\Command;

use PhpSpec\ObjectBehavior;
use Training\Command\AbstractCommand;
use Training\Command\ExitCode;
use Training\Command\GreetingCommand;
use Training\Exception\GreetingException;
use Training\Manager\GreetingManager;
use Training\Output\Output;

class GreetingCommandSpec extends ObjectBehavior
{
    function it_is_initializable(GreetingManager $greeter, Output $output)
    {
        $this->beConstructedWith($output, $greeter);
        $this->shouldHaveType(GreetingCommand::class);
        $this->shouldBeAnInstanceOf(AbstractCommand::class);
    }

    function it_should_output_the_result_of_the_greeter(GreetingManager $greeter, Output $output): void
    {
        $this->beConstructedWith($output, $greeter);

        $greeter->getGreetMessage('Alice', false)
            ->willReturn('Hello Alice!');

        $this->execute('Alice', false)
            ->shouldBeEqualTo(ExitCode::OK);

        $output->writeln('Hello Alice!')
            ->shouldBeCalledOnce();
    }

    function it_should_return_an_error_code(GreetingManager $greeter, Output $output): void
    {
        $this->beConstructedWith($output, $greeter);

        $greeter->getGreetMessage('Alice', false)
            ->willThrow(new GreetingException('An exception :/'));

        $this->execute('Alice', false)
            ->shouldBeEqualTo(ExitCode::ERROR);

        $output->writeln('An error occurred: "An exception :/".')
            ->shouldBeCalledOnce();
    }
}
