<?php

declare(strict_types=1);

namespace spec\Training\Manager;

use PhpSpec\ObjectBehavior;
use Training\Exception\GreetingException;
use Training\Manager\GreetingManager;

class GreetingManagerSpec extends ObjectBehavior
{
    function it_is_initializable(): void
    {
        $this->shouldHaveType(GreetingManager::class);
    }

    function it_should_greet_anonymous_people(): void
    {
        $this->getGreetMessage()->shouldBeEqualTo('Hello!');
    }

    function it_should_greet_named_people(): void
    {
        $this->getGreetMessage('John')->shouldBeEqualTo('Hello John!');
        $this->getGreetMessage('Patrick', false)->shouldBeEqualTo('Hello Patrick!');
    }

    function it_should_yell_at_anonymous_people_when_asking_to(): void
    {
        $this->getGreetMessage('', true)->shouldBeEqualTo('HELLO!');
    }

    function it_should_yell_at_named_people_when_asking_to(): void
    {
        $this->getGreetMessage('John', true)->shouldBeEqualTo('HELLO JOHN!');
    }

    function it_should_not_yell_at_bob(): void {
        foreach (['bob', 'Bob', 'BOb', 'BoB', 'BOB', 'bOb', 'bOB', 'boB'] as $name) {
            $this->shouldThrow(new GreetingException('Dont yell at Bob!'))
                ->during('getGreetMessage', [$name, true]);
        }
    }

    function it_should_greet_bob(): void {
        foreach (['bob', 'Bob', 'BOb', 'BoB', 'BOB', 'bOb', 'bOB', 'boB'] as $name) {
            $this->getGreetMessage($name, false)->shouldBeEqualTo(\sprintf('Hello %s!', $name));
        }
    }
}
