<?php

declare(strict_types=1);

namespace Training\Command;

class DummyCommand extends AbstractCommand
{
    public function execute(): int
    {
        $this->output->writeln('Nothing to see here.. just a dummy command');
        return ExitCode::OK;
    }
}
