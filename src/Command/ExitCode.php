<?php

declare(strict_types=1);

namespace Training\Command;

class ExitCode
{
    public const OK = 0;
    public const ERROR = 1;
}
