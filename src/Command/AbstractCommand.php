<?php

declare(strict_types=1);

namespace Training\Command;

use Training\Output\Output;

abstract class AbstractCommand
{
    protected Output $output;

    public function __construct(Output $output)
    {
        $this->output = $output;
    }

    abstract public function execute(): int;
}
