<?php

declare(strict_types=1);

namespace Training\Output;

/**
 * Do not test for the training.
 */
class Output
{
    private $outputStream;

    public function __construct()
    {
        $this->outputStream = \fopen('php://stdout', 'w');
    }

    public function writeln(string $text): void
    {
        \fwrite($this->outputStream, $text . PHP_EOL);
    }
}
