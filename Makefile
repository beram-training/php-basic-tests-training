D_IMAGE ?= ekino/ci-php:7.4-2019.12.26
D_VOLUME ?= -v "$$PWD":/app:delegated
D_WORKDIR ?= -w /app
D_ARGS ?= --rm $(D_VOLUME) $(D_WORKDIR) $(D_IMAGE)
D_RUN ?= docker run

.PHONY: run
run:
	$(D_RUN) $(D_ARGS) composer install
	$(D_RUN) -it $(D_ARGS) /bin/sh

.PHONY: clean
clean:
	rm -rf bin vendor
