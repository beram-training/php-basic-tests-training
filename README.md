# PHP Basic Tests Training

Provide exercises to start writing tests in PHP.

You'll principally practice [phpunit](https://phpunit.de/)
and a little bit of [phpspec](https://www.phpspec.net/en/stable/).

You'll see that the code is pure PHP: no Symfony, no Laravel, no Drupal etc..
Just PHP :)

## Requirements

- **Docker**

## Installation

```bash
$ make
```

You should be connected to the PHP container and you can start working now :)

## Usage

```bash
$ php cli.php [COMMAND] [ARG1] [ARG2]...
```

For instance:

```bash
$ php cli.php dummy
Nothing to see here.. just a dummy command

$ # The following examples will work when step 1 has been done.
$ php cli.php greet
Hello!
$ php cli.php greet Foo
Hello Foo!
$ php cli.php greet Foo yell
HELLO FOO!
```

## Exercise

**Note:**

> To verify your code, we will use the following commands:
>
> ```bash
> $ make
> $ ./bin/phpspec run
> $ ./bin/phpunit
> ```
>
> **If we need to do something else please tell us :)**

### Step 1

Implement the greeting command.
You should respect the specification defined in the `spec/` folder.

To check if your implementation is correct run:

```bash
$ ./bin/phpspec run
```

Do it incrementally, PHPSpec will help you :)

Start with the `GreetingManager`:

```bash
$ ./bin/phpspec run "Training\Manager\GreetingManager"
```

Read what it tells you, do it then rerun `phpspec` etc..

When you are done all phpspec tests should be green :)

### Step 2

Install and configure PHPUnit.
Your tests should live inside the `tests/` folder.

In order to check that your installation/configuration works you could run
the `ExampleTest` provided:

```bash
$ ./bin/phpunit --filter=ExampleTest
```

Add PHPUnit tests that cover the greeting service and the greeting command.

### Step 3

We want to add a method to greet people according to the time of day:

- `Good morning John`
- `Good afternoon John`
- `Good evening John`
- `Good night John`

Like the default format the name is optional and you can yell at people.
(you can yell at Bob too)

Starts by creating the phpunit tests and then code the method.

## Extra Exercises

Some ideas if you want to play more with tests.

Remember: for each exercise first create the phpunit test(s) and then code ;)

- Create a service to calculate the price of beers according to the number of
  wanted beers.
  The specifications:
  - Base price of a beer: 4€
  - The first 10th beers: 2€
  - Starting from 500 beers: 75% discount

- Create a service to encode/decode a message that contains only
  letters (case insensitive) by replacing the letter with its
  number in the alphabet.
  For instance: A=1, B=2, C=3 etc..
  A space stays a space. A new line stays a new line.
  If the message to encode contains a number throw a logic exception.
  Bonus: Modify your service to be able to define which letter represents the 1.
  For instance: A=24, B=25, C=26, D=1, E=2, F=3 etc..
